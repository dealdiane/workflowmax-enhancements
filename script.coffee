###
// ==UserScript==
// @name        WorkflowMax Power Search
// @namespace   http://userscripts.org/users/74338
// @description Supercharge Workflowmax Select Job Popup
// @include     https://my.workflowmax.com/*
// @version     0.0.6a
// @require     https://dl.dropboxusercontent.com/u/3209117/wfmax/mutation_summary.js
// @require     https://dl.dropboxusercontent.com/u/3209117/wfmax/jquery.mutation-summary.js
// @grant       none
// ==/UserScript==

// CHANGELOG
// 0.0.1a (2013-05-09)
//  Initial Release (alpha)
// 0.0.2a (2013-05-09)
//  Updated script sources
// 0.0.3a (2013-05-10)
//  Fixed: Enter key causes timer to start
// 0.0.4a (2013-05-10)
//  Added support for search tags
//  Added shortcut: ctrl+space
//      ctrl+space shows the task popup or brings the filter textbox to focus if already shown
// 0.0.5a (2013-05-13)
//  Fixed: Filter by task doesn't always work
//  Fixed: Tag textbox doesn't display after updating tags
//  Auto select job/task id from querystring
// 0.0.6a (2013-05-24)
//      Added alert support when timer is paused for some period of time (EXPERIMENTAL)
###

tick = (ms, cb) -> setInterval cb, ms
timeout = (ms, cb) -> setTimeout cb, ms
jobs = null
jobsLoaded = 0
tags = null
formCheckPollId = null
resultTimeoutId = null
alertTimeoutId = null
alertTimeout = 15
lastKeyword = ''
alertTitle = 'Your timer has been paused for too long.'
filterTemplate = 
"""
<div>
<label>Filter:<span id="filter-result"></span></label>
<input id="task-filter" type="text"/>
</div>
<div>
<label id="notify-on-pause-blk">
    <input type="checkbox" id="notify-on-pause" />
    Notify me when I forget to resume my time
    <a id="change-timeout" href="\#">Change timeout</a>
</label>
<small style="display:none;" id="chrome-notify-broken">Notification alert has been disabled. Access to unsafe window not allowed. https://code.google.com/p/chromium/issues/detail?id=222652</small>
</div>
"""
tagTemplate =
"""
<div class="tags" style="margin-top: 10px">
    <div id="current-tags"></div>
    <input type="text" id="task-tags"/>
</div>
<style>
    #filter-result {
        margin-left: 5px;
        color: #777;
        font-style: italic;
    }
    #task-tags {
        display: none;
    }
    #current-tags span {
        padding: 3px;
        margin-right: 5px;
        border-radius: 3px;
        background-color: white;
        border: 1px solid #ccc;
    }
    #timer-paused-popup-background {
        cursor: pointer;
        display: none;
        background-color: #000;
        opacity: 0.2;
        z-index: 999;
        position: absolute;
        width: 100%;
        height: 100%;
        min-height: 100px;
        top: 0;
        left: 0;
    }

    #timer-paused-popup {
        cursor: pointer;
        display: none;
        width: 380px;
        height: 25px;
        padding: 20px 10px;
        font-size: 16px;
        font-weight: bold;
        border: 1px solid #000;
        position: absolute;
        background-color: white;
        left: 50%;
        top: 50%;
        margin-top: -50px;
        margin-left: -380px;
        z-index: 1000;
        border-radius: 3px;
    }
</style>
"""

#test
#test

#test

do ($ = jQuery) ->
    
    containsAllKeywords = (keywords, value) ->
        lowerCaseValue = value.toLowerCase()
        for keyword in keywords.split(/\s/) 
            return false if lowerCaseValue.indexOf(keyword) == -1
        true

    filterJobs = (keywords, isDontUpdateIfNoJobFound) ->
        return if keywords.toLowerCase() is lastKeyword
        lastKeyword = keywords.toLowerCase()
        matchedJobs  = (job for job in jobs when containsAllKeywords keywords, [job.search, tags[job.id + '.']].concat(tags[job.id + '.' + task.id] for task in job.tasks).join(' ') )

        return if isDontUpdateIfNoJobFound and matchedJobs.length is 0

        $('#timerform_new\\[job\\]')
            .find('option')
            .remove()
            .end()
            .append(("<option value=\"#{ job.id }\">#{ job.name }</option>" for job in matchedJobs).join(''))
        
        matchedTasks = []
        for matchedJob in matchedJobs
            for task in matchedJob.tasks when containsAllKeywords keywords, "#{task.name} #{task.id} #{matchedJob.name} #{tags[matchedJob.id + '.' + task.id]}"
                matchedTasks.push { job: matchedJob, task }

        if matchedTasks.length is 1
            matchedTask = matchedTasks[0]
            $('#timerform_new\\[task\\]')
                .find('option')
                .remove()
                .end()
                .append(("<option value=\"#{ task.id }\">#{ task.name }</option>" for task in matchedTask.job.tasks).join(''))
                .val(matchedTask.task.id)
                .change()
        else
            evt = document.createEvent "HTMLEvents"
            evt.initEvent 'change', true, true 
            $('#timerform_new\\[job\\]')[0].dispatchEvent evt

        
        clearTimeout resultTimeoutId if resultTimeoutId isnt null
        return if keywords is ''
        $('#filter-result').text("#{matchedJobs.length} jobs, #{matchedTasks.length} tasks found").stop().show()
        resultTimeoutId = setTimeout (-> $('#filter-result').fadeOut(2000)), 1000
        return

    checkQueryString = ->
        if location.search.length and jobsLoaded is jobs.length
            jobOrTaskId = location.search.match(/id=(\d+)/)
            if jobOrTaskId isnt null
                $('#task-filter').val jobOrTaskId[1]
                filterJobs(jobOrTaskId[1], true)
                    
        return

    loadTasks = (job, resourceId) ->

        $.ajax
            url     : '/ajaxpro/WorkFlowMax.Web.UI.AjaxLib,WorkFlowMax.Web.UI.ashx',
            type    : 'post',
            data    : JSON.stringify(
                job_id: job.id,
                resource_id: resourceId )
            headers : 
                'X-AjaxPro-Method': 'JobTaskListByResource'
            success : (e) ->
                job.tasks  = JSON.parse(e.substr(0, e.length - 3)).tasks.task ? []
                job.search += ' ' + (task.name + ' ' + task.id for task in job.tasks).join(' ')
                sessionStorage.setItem(job.id, JSON.stringify(
                    tasks: job.tasks, 
                    search: job.search
                ))
                return
            complete : ->
                if not job.cached
                    jobsLoaded++
                    checkQueryString()
        return

    loadJobs =->
        resourceId = $('#timer-resource').val()
        jobs = for job in $('#timerform_new\\[job\\] option')
                do (job) ->
                    $job    = $(job)
                    jobId   = $job.attr('value')
                    jobName = $job.text()
                    id: jobId, name: jobName, search: "#{jobId} #{jobName}", tasks: []
        
        for job in jobs
            savedTask = JSON.parse(sessionStorage.getItem(job.id))
            if savedTask isnt null
                job.search = savedTask.search
                job.tasks  = savedTask.tasks
                job.cached = true
                jobsLoaded++
                checkQueryString()

            loadTasks job, resourceId

        return


    onTimerFormChanged = (changes, force) ->
        if not changes.length or not (change for change in changes when change.added.length > 0).length or $('#task-filter').length
            $('#timer-dropdown').mutationSummary 'disconnect'
            return if force != true or $('#task-filter').length

        timeoutId = null

        #init tags
        #tags
        $(tagTemplate).insertBefore '#timer-dropdown .ButtonPanel'

        tags = JSON.parse(localStorage.getItem('tasktags')) or {}

        #init filter
        $(filterTemplate).prependTo '#timer-dropdown .field-container'

        if window.WorkflowMax is undefined
            $('#notify-on-pause-blk').hide()
            $('#chrome-notify-broken').show()

        loadJobs()
        $('#notify-on-pause')
            .each(-> @checked = localStorage.getItem('notifyonpause')?.toLowerCase() is 'true')
            .change( -> 
                localStorage.setItem('notifyonpause', $(@).is(':checked')))

        $('#task-filter')
            .focus()
            .bind 'keydown',
                (e) -> 
                    return if (e.which or e.keyCode) isnt 13
                    e.preventDefault()
                    e.stopPropagation()
                    #$(@).change()
                    $('#timerform_new_btnSave').click()
                    false
            .bind 'change keypress',
                (e) ->
                    clearTimeout timeoutId if timeoutId isnt null
                    $this = $(@)
                    timeoutId = setTimeout \
                        ( -> filterJobs $this.val() ), 500

        # tags logic
        $('#current-tags').click ->
            setTimeout \
                (-> 
                    $('#current-tags').hide()
                    $('#task-tags').show().focus()
                    $(document).bind 'click.tags', \
                    (e) ->
                        if not $(e.target).is('#task-tags') 
                            $(document).unbind('click.tags')
                            saveTags $('#task-tags').val()
                ), 50

        saveTags = (currentTags) ->
            key = [$('#timerform_new\\[job\\]').val(), $('#timerform_new\\[task\\]').val()].join('.')
            selectedTags = $.trim(currentTags)
            tags[key] = selectedTags
            localStorage.setItem 'tasktags', JSON.stringify(tags)
            $('#current-tags').show()
            $('#task-tags').hide()
            updateTags()

        updateTags = ->
            jobId = $('#timerform_new\\[job\\]').val()
            if jobId isnt null and jobId.length isnt 0
                key = [jobId, $('#timerform_new\\[task\\]').val()].join('.')
                selectedTags = tags[key] or ''
                $('#task-tags').val selectedTags
                $('#current-tags').html(("<span>#{tag}</span>" for tag in selectedTags.split(/\s/) when tag.length > 0).join('') || '<span>Click here to add tags</span>')
            else
                $('#current-tags').html('')

        $('#timerform_new\\[job\\], #timerform_new\\[task\\]').change updateTags

        $('#task-tags').keypress (e) ->
            return if (e.which or e.keyCode) isnt 13
            e.preventDefault()
            e.stopPropagation()
            saveTags $(@).val()
            false

        $('#change-timeout').click ->
            currentAlertTimeout = alertTimeout
            while true
                currentAlertTimeout = prompt('Enter new timeout value in minutes', currentAlertTimeout)
                alertTimeout = currentAlertTimeout if currentAlertTimeout isnt null
                break if not isNaN(parseInt(alertTimeout))

            localStorage.setItem('alertTimeout', alertTimeout)
            return false

        return

    onTimeClockClassChange =->
        if $('#menu-timeclock').hasClass('timeclock-paused') and alertTimeoutId is null
            alertTimeoutId = setTimeout alertPaused, alertTimeout * 60000
            $('#notify-on-pause').attr('disabled', 'disabled')
            $('#change-timeout').hide()
        else if alertTimeoutId
            $('#notify-on-pause').removeAttr('disabled')
            $('#change-timeout').show()
            clearTimeout alertTimeoutId
            alertTimeoutId = null
        return

    alertPaused =->
        defTitle = document.title
        alertTimeoutId = null
        titleIndex = 0
        # alert 'Your timer has been paused for too long.' if $('#notify-on-pause').is(':checked')
        if $('#notify-on-pause').is(':checked')
            titleMarqueeId = tick 200, ->
                                document.title = alertTitle.substr(titleIndex) + '   ' + alertTitle.substr(0, titleIndex)
                                if titleIndex >= alertTitle.length then titleIndex = 0 else titleIndex++
                                
            $('#timer-paused-popup-background, #timer-paused-popup').show();
            $('body').live 'click.wfmax', ->
                $('body').die 'click.wfmax'
                clearInterval titleMarqueeId
                document.title = defTitle
                $('#timer-paused-popup-background, #timer-paused-popup').hide();
                return false

    
    # listen for changes in the timer form and only activate the plugin when necessary
    $('#timer-dropdown').mutationSummary 'connect', onTimerFormChanged, [ element: '#timerform_new_form' ]
    # $('#timer-dropdown').mutationSummary 'connect', onTimerFormChanged, [ all: true ]

    # ctrl+space shortcut
    $(document).keypress (e) ->
        if e.ctrlKey and (e.which or e.charCode) is 32
            if $('#task-filter').is(':visible')
                $('#task-filter').focus()
            else
                # toggle timer popup dialog
                if window.WorkflowMax 
                    WorkflowMax.UI.TimerManager.toggle() 
                else 
                    $('#menu-timeclock a:first-child').click()

                    if formCheckPollId is null
                        # Mutation observers doesn't work on non-user initiated events in chrome so we need to call it manually
                        formCheckPollId = tick 500, -> 
                            if $('#timerform_new\\[job\\]').length
                                clearInterval formCheckPollId
                                formCheckPollId = null
                                onTimerFormChanged [], true
            return false

    cachedAlertTimeout = localStorage.getItem('alertTimeout')
    if cachedAlertTimeout isnt null and not isNaN(parseInt(cachedAlertTimeout))
        alertTimeout = cachedAlertTimeout

    # pause alert
    if window.WorkflowMax and WorkflowMax.UI.TimerManager.started
        $('body').append('<div id="timer-paused-popup-background">')
        $('<div id="timer-paused-popup">').text(alertTitle).appendTo('body');
        timerManagerStarted = WorkflowMax.UI.TimerManager.started
        WorkflowMax.UI.TimerManager.started = ->
            $('#menu-timeclock').mutationSummary 'disconnect'
            timerManagerStarted()
            $('#menu-timeclock').mutationSummary 'connect', onTimeClockClassChange, [ { attribute: 'class' } ]

        if $('#menu-timeclock').hasClass('timeclock-running')
            # console.log "what if it's really paused?"
            # what if it's really paused?
            # $('#menu-timeclock').removeClass('timeclock-paused') if $('#menu-timeclock').hasClass('timeclock-paused')
            onTimeClockClassChange()
            $('#menu-timeclock').mutationSummary 'connect', onTimeClockClassChange, [ { attribute: 'class' } ]

        return
    return