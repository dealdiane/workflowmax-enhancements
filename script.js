﻿/*
// ==UserScript==
// @name        WorkflowMax Power Search
// @namespace   http://userscripts.org/users/74338
// @description Supercharge Workflowmax Select Job Popup
// @include     https://my.workflowmax.com/*
// @version     0.0.6a
// @require     https://dl.dropboxusercontent.com/u/3209117/wfmax/mutation_summary.js
// @require     https://dl.dropboxusercontent.com/u/3209117/wfmax/jquery.mutation-summary.js
// @grant       none
// ==/UserScript==

// CHANGELOG
// 0.0.1a (2013-05-09)
//  Initial Release (alpha)
// 0.0.2a (2013-05-09)
//  Updated script sources
// 0.0.3a (2013-05-10)
//  Fixed: Enter key causes timer to start
// 0.0.4a (2013-05-10)
//  Added support for search tags
//  Added shortcut: ctrl+space
//      ctrl+space shows the task popup or brings the filter textbox to focus if already shown
// 0.0.5a (2013-05-13)
//  Fixed: Filter by task doesn't always work
//  Fixed: Tag textbox doesn't display after updating tags
//  Auto select job/task id from querystring
// 0.0.6a (2013-05-24)
//      Added alert support when timer is paused for some period of time (EXPERIMENTAL)
*/

var alertTimeout, alertTimeoutId, alertTitle, filterTemplate, formCheckPollId, jobs, jobsLoaded, lastKeyword, resultTimeoutId, tagTemplate, tags, tick, timeout;

tick = function(ms, cb) {
  return setInterval(cb, ms);
};

timeout = function(ms, cb) {
  return setTimeout(cb, ms);
};

jobs = null;

jobsLoaded = 0;

tags = null;

formCheckPollId = null;

resultTimeoutId = null;

alertTimeoutId = null;

alertTimeout = 15;

lastKeyword = '';

alertTitle = 'Your timer has been paused for too long.';

filterTemplate = "<div>\n<label>Filter:<span id=\"filter-result\"></span></label>\n<input id=\"task-filter\" type=\"text\"/>\n</div>\n<div>\n<label id=\"notify-on-pause-blk\">\n    <input type=\"checkbox\" id=\"notify-on-pause\" />\n    Notify me when I forget to resume my time\n    <a id=\"change-timeout\" href=\"\#\">Change timeout</a>\n</label>\n<small style=\"display:none;\" id=\"chrome-notify-broken\">Notification alert has been disabled. Access to unsafe window not allowed. https://code.google.com/p/chromium/issues/detail?id=222652</small>\n</div>";

tagTemplate = "<div class=\"tags\" style=\"margin-top: 10px\">\n    <div id=\"current-tags\"></div>\n    <input type=\"text\" id=\"task-tags\"/>\n</div>\n<style>\n    #filter-result {\n        margin-left: 5px;\n        color: #777;\n        font-style: italic;\n    }\n    #task-tags {\n        display: none;\n    }\n    #current-tags span {\n        padding: 3px;\n        margin-right: 5px;\n        border-radius: 3px;\n        background-color: white;\n        border: 1px solid #ccc;\n    }\n    #timer-paused-popup-background {\n        cursor: pointer;\n        display: none;\n        background-color: #000;\n        opacity: 0.2;\n        z-index: 999;\n        position: absolute;\n        width: 100%;\n        height: 100%;\n        min-height: 100px;\n        top: 0;\n        left: 0;\n    }\n\n    #timer-paused-popup {\n        cursor: pointer;\n        display: none;\n        width: 380px;\n        height: 25px;\n        padding: 20px 10px;\n        font-size: 16px;\n        font-weight: bold;\n        border: 1px solid #000;\n        position: absolute;\n        background-color: white;\n        left: 50%;\n        top: 50%;\n        margin-top: -50px;\n        margin-left: -380px;\n        z-index: 1000;\n        border-radius: 3px;\n    }\n</style>";

(function($) {
  var alertPaused, cachedAlertTimeout, checkQueryString, containsAllKeywords, filterJobs, loadJobs, loadTasks, onTimeClockClassChange, onTimerFormChanged, timerManagerStarted;
  containsAllKeywords = function(keywords, value) {
    var keyword, lowerCaseValue, _i, _len, _ref;
    lowerCaseValue = value.toLowerCase();
    _ref = keywords.split(/\s/);
    for (_i = 0, _len = _ref.length; _i < _len; _i++) {
      keyword = _ref[_i];
      if (lowerCaseValue.indexOf(keyword) === -1) {
        return false;
      }
    }
    return true;
  };
  filterJobs = function(keywords, isDontUpdateIfNoJobFound) {
    var evt, job, matchedJob, matchedJobs, matchedTask, matchedTasks, task, _i, _j, _len, _len1, _ref;
    if (keywords.toLowerCase() === lastKeyword) {
      return;
    }
    lastKeyword = keywords.toLowerCase();
    matchedJobs = (function() {
      var _i, _len, _results;
      _results = [];
      for (_i = 0, _len = jobs.length; _i < _len; _i++) {
        job = jobs[_i];
        if (containsAllKeywords(keywords, [job.search, tags[job.id + '.']].concat((function() {
          var _j, _len1, _ref, _results1;
          _ref = job.tasks;
          _results1 = [];
          for (_j = 0, _len1 = _ref.length; _j < _len1; _j++) {
            task = _ref[_j];
            _results1.push(tags[job.id + '.' + task.id]);
          }
          return _results1;
        })()).join(' '))) {
          _results.push(job);
        }
      }
      return _results;
    })();
    if (isDontUpdateIfNoJobFound && matchedJobs.length === 0) {
      return;
    }
    $('#timerform_new\\[job\\]').find('option').remove().end().append(((function() {
      var _i, _len, _results;
      _results = [];
      for (_i = 0, _len = matchedJobs.length; _i < _len; _i++) {
        job = matchedJobs[_i];
        _results.push("<option value=\"" + job.id + "\">" + job.name + "</option>");
      }
      return _results;
    })()).join(''));
    matchedTasks = [];
    for (_i = 0, _len = matchedJobs.length; _i < _len; _i++) {
      matchedJob = matchedJobs[_i];
      _ref = matchedJob.tasks;
      for (_j = 0, _len1 = _ref.length; _j < _len1; _j++) {
        task = _ref[_j];
        if (containsAllKeywords(keywords, "" + task.name + " " + task.id + " " + matchedJob.name + " " + tags[matchedJob.id + '.' + task.id])) {
          matchedTasks.push({
            job: matchedJob,
            task: task
          });
        }
      }
    }
    if (matchedTasks.length === 1) {
      matchedTask = matchedTasks[0];
      $('#timerform_new\\[task\\]').find('option').remove().end().append(((function() {
        var _k, _len2, _ref1, _results;
        _ref1 = matchedTask.job.tasks;
        _results = [];
        for (_k = 0, _len2 = _ref1.length; _k < _len2; _k++) {
          task = _ref1[_k];
          _results.push("<option value=\"" + task.id + "\">" + task.name + "</option>");
        }
        return _results;
      })()).join('')).val(matchedTask.task.id).change();
    } else {
      evt = document.createEvent("HTMLEvents");
      evt.initEvent('change', true, true);
      $('#timerform_new\\[job\\]')[0].dispatchEvent(evt);
    }
    if (resultTimeoutId !== null) {
      clearTimeout(resultTimeoutId);
    }
    if (keywords === '') {
      return;
    }
    $('#filter-result').text("" + matchedJobs.length + " jobs, " + matchedTasks.length + " tasks found").stop().show();
    resultTimeoutId = setTimeout((function() {
      return $('#filter-result').fadeOut(2000);
    }), 1000);
  };
  checkQueryString = function() {
    var jobOrTaskId;
    if (location.search.length && jobsLoaded === jobs.length) {
      jobOrTaskId = location.search.match(/id=(\d+)/);
      if (jobOrTaskId !== null) {
        $('#task-filter').val(jobOrTaskId[1]);
        filterJobs(jobOrTaskId[1], true);
      }
    }
  };
  loadTasks = function(job, resourceId) {
    $.ajax({
      url: '/ajaxpro/WorkFlowMax.Web.UI.AjaxLib,WorkFlowMax.Web.UI.ashx',
      type: 'post',
      data: JSON.stringify({
        job_id: job.id,
        resource_id: resourceId
      }),
      headers: {
        'X-AjaxPro-Method': 'JobTaskListByResource'
      },
      success: function(e) {
        var task, _ref;
        job.tasks = (_ref = JSON.parse(e.substr(0, e.length - 3)).tasks.task) != null ? _ref : [];
        job.search += ' ' + ((function() {
          var _i, _len, _ref1, _results;
          _ref1 = job.tasks;
          _results = [];
          for (_i = 0, _len = _ref1.length; _i < _len; _i++) {
            task = _ref1[_i];
            _results.push(task.name + ' ' + task.id);
          }
          return _results;
        })()).join(' ');
        sessionStorage.setItem(job.id, JSON.stringify({
          tasks: job.tasks,
          search: job.search
        }));
      },
      complete: function() {
        if (!job.cached) {
          jobsLoaded++;
          return checkQueryString();
        }
      }
    });
  };
  loadJobs = function() {
    var job, resourceId, savedTask, _i, _len;
    resourceId = $('#timer-resource').val();
    jobs = (function() {
      var _i, _len, _ref, _results;
      _ref = $('#timerform_new\\[job\\] option');
      _results = [];
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        job = _ref[_i];
        _results.push((function(job) {
          var $job, jobId, jobName;
          $job = $(job);
          jobId = $job.attr('value');
          jobName = $job.text();
          return {
            id: jobId,
            name: jobName,
            search: "" + jobId + " " + jobName,
            tasks: []
          };
        })(job));
      }
      return _results;
    })();
    for (_i = 0, _len = jobs.length; _i < _len; _i++) {
      job = jobs[_i];
      savedTask = JSON.parse(sessionStorage.getItem(job.id));
      if (savedTask !== null) {
        job.search = savedTask.search;
        job.tasks = savedTask.tasks;
        job.cached = true;
        jobsLoaded++;
        checkQueryString();
      }
      loadTasks(job, resourceId);
    }
  };
  onTimerFormChanged = function(changes, force) {
    var change, saveTags, timeoutId, updateTags;
    if (!changes.length || !((function() {
      var _i, _len, _results;
      _results = [];
      for (_i = 0, _len = changes.length; _i < _len; _i++) {
        change = changes[_i];
        if (change.added.length > 0) {
          _results.push(change);
        }
      }
      return _results;
    })()).length || $('#task-filter').length) {
      $('#timer-dropdown').mutationSummary('disconnect');
      if (force !== true || $('#task-filter').length) {
        return;
      }
    }
    timeoutId = null;
    $(tagTemplate).insertBefore('#timer-dropdown .ButtonPanel');
    tags = JSON.parse(localStorage.getItem('tasktags')) || {};
    $(filterTemplate).prependTo('#timer-dropdown .field-container');
    if (window.WorkflowMax === void 0) {
      $('#notify-on-pause-blk').hide();
      $('#chrome-notify-broken').show();
    }
    loadJobs();
    $('#notify-on-pause').each(function() {
      var _ref;
      return this.checked = ((_ref = localStorage.getItem('notifyonpause')) != null ? _ref.toLowerCase() : void 0) === 'true';
    }).change(function() {
      return localStorage.setItem('notifyonpause', $(this).is(':checked'));
    });
    $('#task-filter').focus().bind('keydown', function(e) {
      if ((e.which || e.keyCode) !== 13) {
        return;
      }
      e.preventDefault();
      e.stopPropagation();
      $('#timerform_new_btnSave').click();
      return false;
    }).bind('change keypress', function(e) {
      var $this;
      if (timeoutId !== null) {
        clearTimeout(timeoutId);
      }
      $this = $(this);
      return timeoutId = setTimeout((function() {
        return filterJobs($this.val());
      }), 500);
    });
    $('#current-tags').click(function() {
      return setTimeout((function() {
        $('#current-tags').hide();
        $('#task-tags').show().focus();
        return $(document).bind('click.tags', function(e) {
          if (!$(e.target).is('#task-tags')) {
            $(document).unbind('click.tags');
            return saveTags($('#task-tags').val());
          }
        });
      }), 50);
    });
    saveTags = function(currentTags) {
      var key, selectedTags;
      key = [$('#timerform_new\\[job\\]').val(), $('#timerform_new\\[task\\]').val()].join('.');
      selectedTags = $.trim(currentTags);
      tags[key] = selectedTags;
      localStorage.setItem('tasktags', JSON.stringify(tags));
      $('#current-tags').show();
      $('#task-tags').hide();
      return updateTags();
    };
    updateTags = function() {
      var jobId, key, selectedTags, tag;
      jobId = $('#timerform_new\\[job\\]').val();
      if (jobId !== null && jobId.length !== 0) {
        key = [jobId, $('#timerform_new\\[task\\]').val()].join('.');
        selectedTags = tags[key] || '';
        $('#task-tags').val(selectedTags);
        return $('#current-tags').html(((function() {
          var _i, _len, _ref, _results;
          _ref = selectedTags.split(/\s/);
          _results = [];
          for (_i = 0, _len = _ref.length; _i < _len; _i++) {
            tag = _ref[_i];
            if (tag.length > 0) {
              _results.push("<span>" + tag + "</span>");
            }
          }
          return _results;
        })()).join('') || '<span>Click here to add tags</span>');
      } else {
        return $('#current-tags').html('');
      }
    };
    $('#timerform_new\\[job\\], #timerform_new\\[task\\]').change(updateTags);
    $('#task-tags').keypress(function(e) {
      if ((e.which || e.keyCode) !== 13) {
        return;
      }
      e.preventDefault();
      e.stopPropagation();
      saveTags($(this).val());
      return false;
    });
    $('#change-timeout').click(function() {
      var currentAlertTimeout;
      currentAlertTimeout = alertTimeout;
      while (true) {
        currentAlertTimeout = prompt('Enter new timeout value in minutes', currentAlertTimeout);
        if (currentAlertTimeout !== null) {
          alertTimeout = currentAlertTimeout;
        }
        if (!isNaN(parseInt(alertTimeout))) {
          break;
        }
      }
      localStorage.setItem('alertTimeout', alertTimeout);
      return false;
    });
  };
  onTimeClockClassChange = function() {
    if ($('#menu-timeclock').hasClass('timeclock-paused') && alertTimeoutId === null) {
      alertTimeoutId = setTimeout(alertPaused, alertTimeout * 60000);
      $('#notify-on-pause').attr('disabled', 'disabled');
      $('#change-timeout').hide();
    } else if (alertTimeoutId) {
      $('#notify-on-pause').removeAttr('disabled');
      $('#change-timeout').show();
      clearTimeout(alertTimeoutId);
      alertTimeoutId = null;
    }
  };
  alertPaused = function() {
    var defTitle, titleIndex, titleMarqueeId;
    defTitle = document.title;
    alertTimeoutId = null;
    titleIndex = 0;
    if ($('#notify-on-pause').is(':checked')) {
      titleMarqueeId = tick(200, function() {
        document.title = alertTitle.substr(titleIndex) + '   ' + alertTitle.substr(0, titleIndex);
        if (titleIndex >= alertTitle.length) {
          return titleIndex = 0;
        } else {
          return titleIndex++;
        }
      });
      $('#timer-paused-popup-background, #timer-paused-popup').show();
      return $('body').live('click.wfmax', function() {
        $('body').die('click.wfmax');
        clearInterval(titleMarqueeId);
        document.title = defTitle;
        $('#timer-paused-popup-background, #timer-paused-popup').hide();
        return false;
      });
    }
  };
  $('#timer-dropdown').mutationSummary('connect', onTimerFormChanged, [
    {
      element: '#timerform_new_form'
    }
  ]);
  $(document).keypress(function(e) {
    if (e.ctrlKey && (e.which || e.charCode) === 32) {
      if ($('#task-filter').is(':visible')) {
        $('#task-filter').focus();
      } else {
        if (window.WorkflowMax) {
          WorkflowMax.UI.TimerManager.toggle();
        } else {
          $('#menu-timeclock a:first-child').click();
          if (formCheckPollId === null) {
            formCheckPollId = tick(500, function() {
              if ($('#timerform_new\\[job\\]').length) {
                clearInterval(formCheckPollId);
                formCheckPollId = null;
                return onTimerFormChanged([], true);
              }
            });
          }
        }
      }
      return false;
    }
  });
  cachedAlertTimeout = localStorage.getItem('alertTimeout');
  if (cachedAlertTimeout !== null && !isNaN(parseInt(cachedAlertTimeout))) {
    alertTimeout = cachedAlertTimeout;
  }
  if (window.WorkflowMax && WorkflowMax.UI.TimerManager.started) {
    $('body').append('<div id="timer-paused-popup-background">');
    $('<div id="timer-paused-popup">').text(alertTitle).appendTo('body');
    timerManagerStarted = WorkflowMax.UI.TimerManager.started;
    WorkflowMax.UI.TimerManager.started = function() {
      $('#menu-timeclock').mutationSummary('disconnect');
      timerManagerStarted();
      return $('#menu-timeclock').mutationSummary('connect', onTimeClockClassChange, [
        {
          attribute: 'class'
        }
      ]);
    };
    if ($('#menu-timeclock').hasClass('timeclock-running')) {
      onTimeClockClassChange();
      $('#menu-timeclock').mutationSummary('connect', onTimeClockClassChange, [
        {
          attribute: 'class'
        }
      ]);
    }
    return;
  }
})(jQuery);
